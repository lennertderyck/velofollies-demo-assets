### Add this submodule your project
#### Requirements
- Access this repo (submodule)
- Access to the repo that needs these assets (your project), and has git initialized

#### Adding the submodule
Execute this code in the repo that needs to access the assets repo.

```shell
git submodule add https://gitlab.com/lennertderyck/velofollies-demo-assets.git
```

The files inside the gitmodule will be cloned inside your project and are available for use.

### Access files inside node projects
In regular node projects you should have direct access to the files inside the module.

This is not the case for projects with, for example, React. Some projects only allow imports from within the `src/` directory. Therefore you need to add the submodule directory as a dependency to the `package.json` file of your project.

```shell
npm i ./velofollies-demo-assets
```

Now you can import files from inside the submodule.

```js
import logoNegative from 'velofollies-demo-assets/assets/logos/negative.png';
import colors from 'velofollies-demo-assets/variables/colors.json';
```

> Altough the files live inside the `src` directory they are made available directly from the root directory when writing import paths.
>
> So the import path `velofollies-demo-assets/*` redirects to `velofollies-demo-assets/src/*`.

## Working with the shared assets
### The variables
Something about variables.
#### As JSON
When working in React (Native) projects the variables-files in `src/variables` can be imported in JavaScript (or TypeScript) files as regular JSON objects.

```js
// file.js
import colors from 'velofollies-demo-assets/variables/colors.json';
```

#### As CSS variables
These are also available as css variables ready to import into an application.

```jsx
// file.jsx / file.js / file.tsx
import 'velofollies-demo-assets/scss/index.scss';
```

#### Adding variables
Variables can be added by:
- extending an existing file
- creating a new file and adding it to the list of files to parse (`sourceFiles` in `./utils/index.js`)

After adding the variables you need to:
1. run the parser (`node utils/index.js`)
2. push the changes
3. pull the changes in the repo's where the assets are used
4. updating the dependency (`npm update velofollies-demo-assets`) inside that repo so the new changes become available