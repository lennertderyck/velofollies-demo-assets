import fs from 'fs'
import path from 'path';

const generateCssVariableName = (name, value) => {    
    const hasValue = !!value;
    if (hasValue) return `--${name}: ${value};`;
    else return `// Variable '${ name }' was not parsed because it has no value`;
}

const generateCssVariableNamesFromJson = (variables, prefix) => {
    const variableGroups = Object.entries(variables);
    return variableGroups.reduce((acc, [groupName, groupOrValue]) => {
        const isGroup = typeof groupOrValue === 'object';
        
        if (isGroup) {
            const prefixedGroupName = [prefix, groupName].filter(Boolean).join('-');
            const d = Object.entries(groupOrValue).map(([name, value]) => generateCssVariableName(`${prefixedGroupName}-${name}`, value));
            return [...acc, ...d];
        } else {
            const prefixedGroupName = [prefix, groupName].filter(Boolean).join('-');
            const d = generateCssVariableName(prefixedGroupName, groupOrValue);
            return [...acc, d];
        }
    }, []);
}

/**
 * 
 * @param {string[]} variables 
 * @param {Object} config
 * @param {string} config.prefix
 */
const jsonToCssSyntax = (variables, config) => {
    return generateCssVariableNamesFromJson(variables, config?.prefix);
}

const wrapCssSyntax = (variablesContent) => {
    return `:root {\n${variablesContent}\n}`
}

/**
 * 
 * @param {...string} content 
 */
export const combineCssFileContent = (...content) => {
    const combinedContent = content.join('\n');
    return wrapCssSyntax(combinedContent)
}

/**
 * @param {Object} config
 * @param {string} config.input Path to the json file that should be converted
 * @param {string} config.variablePrefix
 */
export const generateCssFileContentFromJson = (config) => {
    // Get content os json file in JSON format
    const json = JSON.parse(fs.readFileSync(config.input, 'utf8'));
    
    // Convert all the json entries to CSS variables
    const rawVariables = jsonToCssSyntax(json, { prefix: config?.variablePrefix });
    
    // Add insets (tabs) and newlines
    return rawVariables.map(v => `\t${v}`).join('\n')
}

/**
 * @param {string} fileContent
 * @param {Object} config
 * @param {string} config.output
 * @param {string} config.filename
 * @param {'css'|'scss'} config.outputType
 */
export const writeCssFiles = (fileContent, config) => {
    const outputType = config.outputType || 'scss';
    
    const fileDirectory = path.resolve(config.output);
    const filePath = `${fileDirectory}/${config.filename}.${outputType}`
    
    fs.mkdirSync(fileDirectory, { recursive: true });
    fs.writeFileSync(filePath, fileContent, { encoding: 'utf-8', flag: 'w' });
}

/**
 * @param {Object} config
 * @param {string} config.input Path to the json file that should be converted
 * @param {string} config.output
 * @param {'css'|'scss'} config.outputType
 */
export const generateCssFileFromJsonFilePath = (config) => {
    const escapedContent = generateCssFileContentFromJson(config)
    
    // Wrap the css variables with the needed css syntax
    const fileContent = wrapCssSyntax(escapedContent);
    
    const fileName = path.parse(config.input).name;
    
    writeCssFiles(fileContent, {
        filename: fileName,
        output: config.output,
        outputType: config.outputType
    })
}