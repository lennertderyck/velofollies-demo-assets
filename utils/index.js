import { combineCssFileContent, generateCssFileContentFromJson, generateCssFileFromJsonFilePath, writeCssFiles } from "./parsers/css-variables.js";

const sourceFiles = [
    { input: process.cwd() + '/src/variables/colors.json' },
    { input: process.cwd() + '/src/variables/radius.json', variablePrefix: 'radius' }
]

const generatedCssFileContent = sourceFiles.map(generateCssFileContentFromJson)

writeCssFiles(
    combineCssFileContent(...generatedCssFileContent), 
    { output: process.cwd() + '/src/scss', filename: 'index' }
);